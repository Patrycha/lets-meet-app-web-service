package com.springmvc.restfull.webservice.validation;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = SexValidator.class)
@Documented
public @interface SexConstraint {
    String message() default "Invalid sex character";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
