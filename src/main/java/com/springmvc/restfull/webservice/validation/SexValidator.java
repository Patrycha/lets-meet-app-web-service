package com.springmvc.restfull.webservice.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SexValidator implements ConstraintValidator<SexConstraint, Character> {

    @Override
    public void initialize(SexConstraint sexConstraint) {

    }

    @Override
    public boolean isValid(Character character, ConstraintValidatorContext constraintValidatorContext) {
        if ( character == null )
            return true;

        if ( character.charValue() == 'F' || character.charValue() == 'M')
            return true;
        else
            return false;

    }
}
