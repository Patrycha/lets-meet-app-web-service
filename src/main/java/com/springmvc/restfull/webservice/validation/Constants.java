package com.springmvc.restfull.webservice.validation;

public class Constants {

    public final static int USER_LOGIN_LENGTH_MAX = 30;
    public final static int USER_PASSW_LENGTH_MAX = 30;
    public final static int USER_EMAIL_LENGTH_MAX = 255;
    public final static int USER_NAME_LENGTH_MAX = 30;
    public final static int USER_SURNAME_LENGTH_MAX = 30;
    public final static int USER_DESCRIPTION_LENGTH_MAX = 255;

    public final static int CATEGORY_NAME_LENGTH_MAX = 50;
    public final static int CATEGORY_DESCRIPTION_LENGTH_MAX = 255;


}
