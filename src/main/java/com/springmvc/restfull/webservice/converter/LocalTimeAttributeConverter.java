package com.springmvc.restfull.webservice.converter;

import org.joda.time.LocalTime;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Time;

@Converter(autoApply = false)
public class LocalTimeAttributeConverter implements AttributeConverter<LocalTime, Time>{

    @Override
    public Time convertToDatabaseColumn(LocalTime localTime) {
        //return (localTime == null ? null : Time.valueOf(localTime));
        return (localTime == null ? null : new Time(localTime.toDateTimeToday().getMillis()));
    }

    @SuppressWarnings("deprecation")
    @Override
    public LocalTime convertToEntityAttribute(Time time) {
        //return (time == null ? null : LocalTime.of(time.getHours(), time.getMinutes()));
       return (time == null ? null : new LocalTime(time.getTime()));
    }

}