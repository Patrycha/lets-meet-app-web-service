package com.springmvc.restfull.webservice.service;

import com.springmvc.restfull.webservice.model.City;
import org.springframework.stereotype.Service;

@Service
public interface ICityService {

    City findCityById(long id);
}
