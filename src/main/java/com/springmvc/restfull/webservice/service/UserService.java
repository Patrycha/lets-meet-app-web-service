package com.springmvc.restfull.webservice.service;

import com.springmvc.restfull.webservice.model.*;
import com.springmvc.restfull.webservice.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.sql.Date;
import java.util.*;
import java.util.logging.Logger;

@Service("userService")
public class UserService implements IUserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    CityRepository cityRepository;

    @Autowired
    NotificationRepository notificationRepository;

    @Autowired
    NotificationStatusRepository notificationStatusRepository;

    @Transactional
    public void addUser(User user) {
        userRepository.save(user);
    }

    public Iterable<User> getAllUsersFromRepo(){
       return userRepository.findAll();
    }

    @Override
    public boolean isSubscribing(long userId, long groupId){
       User user = userRepository.findOne(userId);
       if(user == null)
           return false;

       List<Group> list = user.getJoinedGroups();
       for ( Group el : list){
           if(el.getId() == groupId)
               return true;
       }
       return false;
    }

    @Override
    public boolean isOrganizer(long userId, long groupId){
        Group group = groupRepository.findOne(groupId);
        if(group == null)
            return false;

        if(group.getAdministrator().getId() == userId)
            return true;

        return false;
    }

    @Override
    public ResponseEntity<Void> setSubscribtion(long userId, long groupId) {
        User user = userRepository.findOne(userId);
        if (user == null)
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);

        List<Group> joinedGroupsgroups = user.getJoinedGroups();
            for (Group el : joinedGroupsgroups) {
                if (el.getId() == groupId)
                    return new ResponseEntity<Void>(HttpStatus.CONFLICT);
            }


        List<Group> administeredGroups = user.getAdministeresGroups();
            for (Group el : administeredGroups) {
                if (el.getId() == groupId)
                    return new ResponseEntity<Void>(HttpStatus.CONFLICT);
            }


        Group group = groupRepository.findOne(groupId);
        if(group == null)
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);

        joinedGroupsgroups.add(group);

        userRepository.save(user);

        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @Override
    public boolean unsetSubscribtion(long userId, long groupId) {
        User user = userRepository.findOne(userId);
        List<Group> groups = user.getJoinedGroups();

        if (!isSubscribing(userId, groupId))
            return false;

        Logger.getLogger("unsetSubscibtion").info("groups size before: " + userRepository.findOne(userId).getJoinedGroups().size());

        groups.remove(groupRepository.findOne(groupId));
        userRepository.save(user);

        Logger.getLogger("unsetSubscibtion").info("groups size after : " + userRepository.findOne(userId).getJoinedGroups().size());

        return true;
    }

    @Override
    public Iterable<Group> getSubscribingGroups(long categoryId, long userId) {
        List<Group> result = new ArrayList<>();

        User user = userRepository.findOne(userId);
        List<Group> groups=user.getJoinedGroups();
        for(Group el: groups){
            if(el.getCategory().getId() == categoryId)
                result.add(el);
        }
        return result;
    }

    @Override
    public ResponseEntity<ParticipationStatus> isParticipating(long eventId, long userId) {
        //List<Participation> list=userRepository.findOne(userId).getParticipations();
        Event event = eventRepository.findOne(eventId);
        if(event == null)
            return new ResponseEntity<ParticipationStatus>(HttpStatus.NOT_FOUND);

        List<Participation> list = event.getParticipations();
        Participation el = findParticipation(list, userId);
        return el == null? new ResponseEntity<ParticipationStatus>(HttpStatus.NOT_FOUND) : new ResponseEntity<ParticipationStatus>(el.getStatus(), HttpStatus.OK);
    }

    @Override
    public User getUserById(long userId) {
        return userRepository.findOne(userId);
    }

    @Override
    public User login(String header) {
        String base;
        String login;
        String password;
        User user;

        String encoded = header.substring(6);//"Basic " + ...
        byte[] decoded =  Base64.getDecoder().decode(encoded);
        String text = new String (decoded, StandardCharsets.UTF_8);

        int index = text.indexOf(':');
        if(index == -1)
            return null;

        login = text.substring(0,index);
        password = text.substring( index + 1);
        user = userRepository.findByLogin(login);
        if(user == null)
            return null;

        if (user.getPassword().equals(password))
            return user;

        return null;
    }

    @Override
    public User register(User user) {
        //check unique values
        if( userRepository.findByLogin(user.getLogin()) != null || userRepository.findByEmail(user.getEmail()) != null){
            return null;
        }

        //set the other fields
        user.setAdmin(false);
        user.setJoiningDate(new Date(Calendar.getInstance().getTimeInMillis()));
        user.setAbsencesCount(0);
        user.setAverage(0);
        user.setCancellingsCount(0);
        user.setGradesCount(0);

        user.setCity(cityRepository.findOne((long)1));

        //save
        userRepository.save(user);
        User result = userRepository.findByLogin(user.getLogin());

        return result;
    }

    @Override
    public ResponseEntity<List<Notification>> getNotifications(long userId, String statusName) {
        //HttpHeaders headers = new HttpHeaders();
        //headers.add("Content-Type", "application/json");

        User user = userRepository.findOne(userId);
        if (user == null)
            return new ResponseEntity<List<Notification>>(new ArrayList<>(0), HttpStatus.NOT_FOUND);

        List<Notification> notifications = user.getNotifications();
        if ( notifications == null || notifications.isEmpty())
            return new ResponseEntity<List<Notification>>(new ArrayList<>(0), HttpStatus.OK);

        Iterator<Notification> iterator;
        for (iterator = notifications.iterator(); iterator.hasNext(); ){
            Notification notification = iterator.next();
            if ( notification.getStatus().getName().equals("cancelled"))
                iterator.remove();
        }
        return new ResponseEntity<List<Notification>>(notifications, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<Group>> getGroups(long userId, boolean admin, boolean participant, long categoryID) {
        User user = userRepository.findOne(userId);
        List<Group> result = new ArrayList<>(0);
        if(user == null)
            return new ResponseEntity<List<Group>>(result, HttpStatus.NOT_FOUND);

        if(admin)
            result.addAll(user.getAdministeresGroups());
        if(participant)
            result.addAll(user.getJoinedGroups());

        if(categoryID != -1){ //-1 -> all categories
            for (Iterator<Group> it = result.iterator(); it.hasNext(); ) {
                Group g = it.next();
                if (g.getCategory().getId() != categoryID)
                    it.remove();
            }
        }
        return new ResponseEntity<List<Group>>(result, HttpStatus.OK);
    }

    private Participation findParticipation(List<Participation> participations, long userId) {
        for(Participation el : participations){
            if(el.getUser().getId() == userId)
                return el;
        }
        return null;
    }
}
