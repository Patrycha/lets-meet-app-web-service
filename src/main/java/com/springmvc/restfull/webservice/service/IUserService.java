package com.springmvc.restfull.webservice.service;

import com.springmvc.restfull.webservice.model.Group;
import com.springmvc.restfull.webservice.model.Notification;
import com.springmvc.restfull.webservice.model.ParticipationStatus;
import com.springmvc.restfull.webservice.model.User;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IUserService {

    Iterable<User> getAllUsersFromRepo();

    boolean isSubscribing(long userId, long groupId);

    boolean isOrganizer(long userId, long groupId);

    ResponseEntity<Void> setSubscribtion(long userId, long groupId);

    boolean unsetSubscribtion(long userId, long groupId);

    Iterable<Group> getSubscribingGroups(long categoryId, long userId);

    ResponseEntity<ParticipationStatus> isParticipating(long eventId, long userId);

    User getUserById(long userId);

    User login(String header);

    User register(User user);

    ResponseEntity<List<Notification>> getNotifications(long userId, String statusName);

    ResponseEntity<List<Group>> getGroups(long userId, boolean admin, boolean participant, long categoryID);
}
