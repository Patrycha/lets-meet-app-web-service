package com.springmvc.restfull.webservice.service;

import com.springmvc.restfull.webservice.model.Notification;
import com.springmvc.restfull.webservice.model.NotificationStatus;
import com.springmvc.restfull.webservice.repositories.NotificationRepository;
import com.springmvc.restfull.webservice.repositories.NotificationStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("notificationService")
public class NotificationService implements INotificationService {

    @Autowired
    NotificationRepository notificationRepository;

    @Autowired
    NotificationStatusRepository notificationStatusRepository;

    @Override
    public ResponseEntity<Notification> cancelNotification(long notificationId) {
        Notification notification = notificationRepository.findOne(notificationId);
        if (notification == null)
            return new ResponseEntity<Notification>(HttpStatus.NOT_FOUND);

        NotificationStatus status = notificationStatusRepository.findByName("cancelled");
        if(status == null)
            return new ResponseEntity<Notification>(HttpStatus.NOT_FOUND);

        notification.setStatus(status);
        notificationRepository.save(notification);
        return new ResponseEntity<Notification>(notification, HttpStatus.OK);
    }

}
