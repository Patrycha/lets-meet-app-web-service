package com.springmvc.restfull.webservice.service;

import com.springmvc.restfull.webservice.model.*;
import com.springmvc.restfull.webservice.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service
public class EventService implements IEventService{

    final private String pending = "pending";
    final private String accepted = "accepted";
    final private String cancelled = "cancelled";
    final private String declined = "declined";
    final private String invited = "invited"; //not in db
    final private String present_notAffirmed = "present-not affirmed";
    final private String absent_affirmed = "absent-affirmed";

    @Autowired
    EventRepository eventRepository;

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ParticipationStatusRepository statusRepository;

    @Autowired
    ParticipationRepository participationRepository;

    @Autowired
    VisibilityStatusRepository visibilityStatusRepository;

    @Autowired
    ParticipationStatusRepository participationStatusRepository;

    @Autowired
    NotificationStatusRepository notificationStatusRepository;

    @Autowired
    NotificationRepository notificationRepository;


    public Participation findParticipation(List<Participation> participationList, long eventId){
        for(Participation el : participationList){
            if (el.getEvent().getId() == eventId)
                return el;
        }
        return null;
    }

    @Override
    public ResponseEntity<Participation>  findParticipation(long eventId, long userId){
        User user = userRepository.findOne(userId);
        if (user == null) {
            return new ResponseEntity<Participation>(HttpStatus.NOT_FOUND);
        }
        List<Participation> participations = user.getParticipations();
        if(participations == null)
            return new ResponseEntity<Participation>(HttpStatus.NOT_FOUND);

        Participation participation = findParticipation(participations, eventId);
        if(participation == null)
            return new ResponseEntity<Participation>(new Participation(0,null, null, null, null, null),
                    HttpStatus.OK);

        return new ResponseEntity<Participation>(participation, HttpStatus.OK);
    }

    public Participation  findParticipation2(long eventId, long userId){
        //Iterable<Participation> list = participationRepository.findAll();
        User user = userRepository.findOne(userId);
        if (user == null) {
           return null;
        }

        List<Participation> participations = user.getParticipations();
        if(participations == null)
            return null;

        return findParticipation(participations, eventId);
    }

    @Override
    public Iterable<User> findParticipants(long eventId, String status) {
        Event event = eventRepository.findOne(eventId);
        if(event == null)
            return null;

        List<Participation> participations = event.getParticipations();
        if (participations == null )
            return null;

        List<User> users = new ArrayList<>();
        if (status.equals(pending) ){
            for(int i = 0; i < participations.size(); i++) {
                Participation temp = participations.get(i);
                if (temp.getStatus().getName().equals(status) && temp.getChangeUser().getId() == temp.getUser().getId())
                    users.add(temp.getUser());
            }
        }
        else if (status.equals(invited)){
            for(int i = 0; i < participations.size(); i++) {
                Participation temp = participations.get(i);
                if (temp.getStatus().getName().equals(pending) && temp.getChangeUser().getId() != temp.getUser().getId())
                    users.add(temp.getUser());
            }
        }else{
            for(int i = 0; i < participations.size(); i++) {
                Participation temp = participations.get(i);
                if (temp.getStatus().getName().equals(status))
                    users.add(temp.getUser());
            }
        }

        return users;
    }

    @Override
    public boolean addNewEvent(Event event, long groupId, long userId) {

        Event newEvent = event;
        newEvent.setCreatingDate(new Date(java.util.Calendar.getInstance().getTime().getTime()));
        newEvent.setId(0); //for increment and new object, not update existing one

        User organizer = userRepository.findOne(userId);
        Group group = groupRepository.findOne(groupId);
        VisibilityStatus status = visibilityStatusRepository.findByName(event.getVisibility().getName());

        if(organizer == null || group == null || status == null)
            return false;

        newEvent.setOrganizer(organizer);
        newEvent.setGroup(group);
        newEvent.setVisibility(status);
        eventRepository.save(newEvent);

        generateNotifications(group, newEvent.getName());//
        return true;
    }

    private void generateNotifications(Group group, String eventName) {
        List<User> subscribers = group.getSubscribers();
        Event event = eventRepository.findByName(eventName);
        NotificationStatus status = notificationStatusRepository.findByName("created");
        User admin = group.getAdministrator();

        for ( int i = 0; i < subscribers.size(); i++){
            if(subscribers.get(i).getId() != event.getOrganizer().getId() )
                notificationRepository.save(new Notification(status, event, subscribers.get(i)));
        }

        if(admin.getId() != event.getOrganizer().getId() )
            notificationRepository.save(new Notification(status, event, admin));

    }

    @Override
    public ResponseEntity<ParticipationStatus> changeParticipationStatus(long userId, long changerId, String statusName, long eventId) {
        Participation participation = findParticipation2(eventId, userId);
        if(participation == null)
            return new ResponseEntity<ParticipationStatus>(HttpStatus.NOT_FOUND);
        User changer = userRepository.findOne(changerId);
        ParticipationStatus status = participationStatusRepository.findByName(statusName);

        if(changer == null || status == null) {
            return new ResponseEntity<ParticipationStatus>(HttpStatus.NOT_FOUND);
        }
        Event event = eventRepository.findOne(eventId);
        if(event.getParticipationsCount() == event.getParticipantsLimit() &&
                statusName.equals(accepted))
            return new ResponseEntity<ParticipationStatus>(HttpStatus.CONFLICT);

        participation.setChangeUser(changer);
        participation.setStatus(status);
        participation.setDate(new Date(Calendar.getInstance().getTimeInMillis()));
        participationRepository.save(participation);

        return new ResponseEntity<ParticipationStatus>(participation.getStatus(), HttpStatus.OK);
    }

    @Override
    public Boolean isEventAdmin(long userId, long eventId) {
        User user = userRepository.findOne(userId);
        if (user == null)
            return false;

        List<Event> events = user.getOrganizedEvents();
        if(events == null || events.isEmpty())
            return false;

        for (Event e : events){
            if (e.getId() == eventId)
                return true;
        }
        return false;
    }

    @Override
    public ParticipationStatus requestForParticipation(long eventId, long userId) {
        //String currentStatus="";
        List<Participation> participationList = userRepository.findOne(userId).getParticipations();

        Participation participation = findParticipation(participationList, eventId);
        if(participation == null) {
            //add new row to table
            participation = new Participation();
            participation.setUser(userRepository.findOne(userId));
            participation.setChangeUser(userRepository.findOne(userId));
            participation.setEvent(eventRepository.findOne(eventId));
            participation.setStatus(statusRepository.findByName(pending));
            participation.setDate(new Date(Calendar.getInstance().getTimeInMillis()));
            participationRepository.save(participation);
            //currentStatus = pending;
        }else if(participation.getStatus().getName().equals(cancelled)){
            //update columns
            participation.setStatus(statusRepository.findByName(pending));
            participation.setChangeUser(userRepository.findOne(userId));
            participation.setDate(new Date(Calendar.getInstance().getTimeInMillis()));
            participationRepository.save(participation);

           // currentStatus = pending;
        }
        else if(participation.getStatus().getName().equals(declined)){
         //   currentStatus =  declined;
        }
        else if(participation.getStatus().getName().equals(pending)){
            // request already exists
            if(participation.getChangeUser().getId() == userId){
                participation.setStatus(statusRepository.findByName(cancelled));
           //     currentStatus = cancelled;
            }else{
                participation.setStatus(statusRepository.findByName(accepted));
            //    currentStatus = accepted;
            }
            participation.setChangeUser(userRepository.findOne(userId));
            participation.setDate(new Date(Calendar.getInstance().getTimeInMillis()));
            participationRepository.save(participation);
        }
        else if (participation.getStatus().getName().equals(accepted)){
            //update columns
            if(!isEventAdmin(userId, eventId)) {//todo: test this  if condition
                participation.setStatus(statusRepository.findByName(cancelled));
                participation.setChangeUser(userRepository.findOne(userId));
                participation.setDate(new Date(Calendar.getInstance().getTimeInMillis()));
                participationRepository.save(participation);
                // currentStatus =  cancelled;
            }
        }
        return participation.getStatus();
    }

    @Override
    public List<Participation> getParticipationMyAndToGrade(long userId){
        List<Participation> result;

        User user = userRepository.findOne(userId);
        if (user == null)
            return null;

        List<Participation> participations = user.getParticipations();
        if(participations == null || participations.isEmpty())
            return null;

        List<Event> events = findAcceptedParticipationsFromPast(participations);
        if(events == null)
            return null;

        result = new ArrayList<>();
        for ( int i = 0 ; i < events.size() ; i++){
            List<Participation> participations1 = events.get(i).getParticipations();
            if(participations1.get(i).getStatus().getName().equals(accepted) || participations1.get(i).getStatus().getName().equals(present_notAffirmed)
                    || participations1.get(i).getUser().getId() == userId){

                result.add(participations1.get(i));
            }
        }

        return result;
    }

    @Override
    public Iterable<Event> getEventsToGrade(long userId) {
        List<Participation> result;

        User user = userRepository.findOne(userId);
        if (user == null)
            return null;

        List<Participation> participations = user.getParticipations();
        if(participations == null || participations.isEmpty())
            return null;

        List<Event> events = findAcceptedParticipationsFromPast(participations);

        return events;
    }

    private List<Event> findAcceptedParticipationsFromPast(List<Participation> participations) {
        List<Event> result = new ArrayList<>();

        if(participations == null)
            return null;

        for(int i = 0; i < participations.size(); i++){
            Participation p = participations.get(i);
            boolean statusOk = !p.getStatus().getName().equals(pending) && !p.getStatus().getName().equals(declined) && !p.getStatus().getName().equals(cancelled)
                    && !p.getStatus().getName().equals(absent_affirmed);
            boolean dateBefore = p.getEvent().getEndingDate().toLocalDate().isBefore(new Date(Calendar.getInstance().getTimeInMillis()).toLocalDate());
            boolean dateEqual = p.getEvent().getEndingDate().toLocalDate().isEqual(LocalDate.now());
            boolean timeBefore = p.getEvent().getEndingTime().toLocalTime().isBefore(LocalTime.now());

           if(statusOk && (dateBefore || (dateEqual && timeBefore))){
               result.add(p.getEvent());//todo: w przyszłości usunac powtarzające się el, ale i tak ich tu nie bedzie w tym zastosowaniu
            }
        }
        return result;
    }
}
