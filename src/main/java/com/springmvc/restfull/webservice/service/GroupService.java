package com.springmvc.restfull.webservice.service;

import com.springmvc.restfull.webservice.model.Category;
import com.springmvc.restfull.webservice.model.Event;
import com.springmvc.restfull.webservice.model.Group;
import com.springmvc.restfull.webservice.model.User;
import com.springmvc.restfull.webservice.repositories.CategoryRepository;
import com.springmvc.restfull.webservice.repositories.GroupRepository;
import com.springmvc.restfull.webservice.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("groupService")
@Transactional
public class GroupService implements IGroupService{

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    UserRepository userRepository;

    @Override
    public Iterable<Group> findAllGroups() {
        return groupRepository.findAll();
    }

    @Override
    public Group findGroupById(long id) {
        return groupRepository.findOne(id);
    }

    @Override
    public List<User> findUsers(long groupId){
        Group group = groupRepository.findOne(groupId);

        return group != null ? group.getSubscribers() : null;
    }

    @Override
    public ResponseEntity<Void> addNewGroup(long categoryId, Group newGroup, long userId) {
        Category category = categoryRepository.findOne(categoryId);
        if(category == null)
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        newGroup.setCategory(category);

        User user = userRepository.findOne(userId);
        if(user == null)
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        newGroup.setAdministrator(user);

        groupRepository.save(newGroup);

        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @Override
    public Iterable<Event> findEvents(long groupId) {
        Group group = groupRepository.findOne(groupId);
        return group != null ? group.getEvents() : null;
    }

    @Override
    public Integer countSubscribers(long groupId) {
        Group group = groupRepository.findOne(groupId);
        return group == null? null : group.getSubscribers().size();
    }

    @Override
    public Integer countEvents(long groupId) {
        Group group = groupRepository.findOne(groupId);
        return group == null? null : group.getEvents().size();
    }

}
