package com.springmvc.restfull.webservice.service;

import com.springmvc.restfull.webservice.model.Event;
import com.springmvc.restfull.webservice.model.Participation;
import com.springmvc.restfull.webservice.model.ParticipationStatus;
import com.springmvc.restfull.webservice.model.User;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface IEventService {

    ParticipationStatus requestForParticipation(long eventId, long userId);

    ResponseEntity<Participation> findParticipation(long eventId, long userId);

    Iterable<User> findParticipants(long eventId, String status);

    boolean addNewEvent(Event event, long groupId, long userId);

    ResponseEntity<ParticipationStatus> changeParticipationStatus(long userId, long changerId, String statusName, long eventId);

    Boolean isEventAdmin(long userId, long eventId);

    Iterable<Participation> getParticipationMyAndToGrade(long userId);

    Iterable<Event> getEventsToGrade(long userId);
}
