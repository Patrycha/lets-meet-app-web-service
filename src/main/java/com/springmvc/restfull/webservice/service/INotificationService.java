package com.springmvc.restfull.webservice.service;

import com.springmvc.restfull.webservice.model.Notification;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface INotificationService {

    ResponseEntity<Notification> cancelNotification(long notificationId);
}
