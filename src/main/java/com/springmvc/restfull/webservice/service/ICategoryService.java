package com.springmvc.restfull.webservice.service;

import com.springmvc.restfull.webservice.model.Category;
import org.springframework.stereotype.Service;

@Service
public interface ICategoryService {

    Iterable<Category> findAllCategories();

    Category findById(long id);

    void addCategory(Category category);

}
