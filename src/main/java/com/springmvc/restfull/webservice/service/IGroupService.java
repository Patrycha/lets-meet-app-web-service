package com.springmvc.restfull.webservice.service;

import com.springmvc.restfull.webservice.model.Event;
import com.springmvc.restfull.webservice.model.Group;
import com.springmvc.restfull.webservice.model.User;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IGroupService {

    Iterable<Group> findAllGroups();

    Group findGroupById(long id);

    List<User> findUsers(long id);

    ResponseEntity<Void> addNewGroup(long categoryId, Group newGroup, long userId);

    Iterable<Event> findEvents(long groupId);

    Integer countSubscribers(long groupId);

    Integer countEvents(long groupId);
}
