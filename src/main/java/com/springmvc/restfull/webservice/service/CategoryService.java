package com.springmvc.restfull.webservice.service;

import com.springmvc.restfull.webservice.model.Category;
import com.springmvc.restfull.webservice.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("categoryService")
@Transactional
public class CategoryService implements ICategoryService{

    @Autowired
    CategoryRepository categoryRepository;

    public Iterable<Category> findAllCategories() {
        return categoryRepository.findAll();
    }

    @Override
    public Category findById(long id) {
        return categoryRepository.findOne(id);
    }

    @Override
    public void addCategory(Category category) {
        categoryRepository.save(category);
    }

}
