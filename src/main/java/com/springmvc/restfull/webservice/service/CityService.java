package com.springmvc.restfull.webservice.service;

import com.springmvc.restfull.webservice.model.City;
import com.springmvc.restfull.webservice.repositories.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CityService implements ICityService{

    @Autowired
    CityRepository cityRepository;

    public City findCityById(long id){
        return cityRepository.findOne(id);
    }
}
