package com.springmvc.restfull.webservice.controller;

import com.springmvc.restfull.webservice.model.Category;
import com.springmvc.restfull.webservice.model.Group;
import com.springmvc.restfull.webservice.service.ICategoryService;
import com.springmvc.restfull.webservice.service.ICityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CategoryController {

    @Autowired
    ICategoryService categoryService;

    @Autowired
    ICityService cityService;

    @GetMapping(path="/category/")
    public Iterable<Category> getAllCategories(){
           return categoryService.findAllCategories();
       }

    @GetMapping(path="/category/{id}/")
    public Category getCategoryById(@PathVariable("id") long id){
        return categoryService.findById(id);
    }

    @GetMapping(path="/category/{id}/group/")
    public List<Group> getGroupsByCategoryId(@PathVariable("id") long categoryId){
        return getCategoryById(categoryId).getGroups();
    }

    @PostMapping(path = "/addCategory/")
    public void addCategory(){
        categoryService.addCategory(new Category((long)9,"Nazwa", "Opis", "Zdjecie", cityService.findCityById(1)));
    }
}
