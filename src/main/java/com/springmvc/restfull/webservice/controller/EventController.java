package com.springmvc.restfull.webservice.controller;

import com.springmvc.restfull.webservice.model.Event;
import com.springmvc.restfull.webservice.model.Participation;
import com.springmvc.restfull.webservice.model.ParticipationStatus;
import com.springmvc.restfull.webservice.model.User;
import com.springmvc.restfull.webservice.repositories.EventRepository;
import com.springmvc.restfull.webservice.service.IEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class EventController {
    @Autowired
    IEventService eventService;
    @Autowired
    EventRepository eventRepository;

    @PostMapping(path = "/event/{eid}/user/{uid}/")
    public ParticipationStatus setParticipation(@PathVariable("eid") long eventId, @PathVariable("uid") long userId){
       return eventService.requestForParticipation(eventId, userId);
    }

    @GetMapping(path = "/event/{eid}/")
    public Event getEvent(@PathVariable("eid") long eventId){
        return eventRepository.findOne(eventId);
    }

    @GetMapping(path = "/event/{eid}/participation/user/{uid}/")
    public ResponseEntity<Participation> getParticipation(@PathVariable("uid") long userId, @PathVariable("eid") long eventId){
        return eventService.findParticipation(eventId, userId);
    }

    @GetMapping(path = "/event/{eid}/user/")
    public Iterable<User> getParticipants(@PathVariable("eid") long eventId, @RequestParam("status") String statusName){
        return eventService.findParticipants(eventId, statusName);
    }

    @PostMapping(path = "/group/{gid}/user/{uid}/")
    public boolean addNewEvent(@PathVariable("uid") long userId, @PathVariable("gid") long groupId, @RequestBody Event event){
        return eventService.addNewEvent(event, groupId, userId);
    }

    @PostMapping(path = "/event/{eid}/user/{uid}/changestatus/")
    public ResponseEntity<ParticipationStatus> changeParticipation(@PathVariable("uid") long userId, @PathVariable("eid") long eventId, @RequestParam("changer") long changerId, @RequestParam("status") String statusName){
        return eventService.changeParticipationStatus(userId, changerId, statusName, eventId);
    }

    @GetMapping("/event/{eid}/user/{uid}/isAdmin/")
    public Boolean isEventAdmin(@PathVariable("eid") long eventId, @PathVariable("uid") long userId){
        return eventService.isEventAdmin(userId, eventId);
    }

    @GetMapping("/user/{uid}/grade/")
    public Iterable<Participation> participationsWithRaterToGrade(@PathVariable("uid") long userId){
        return eventService.getParticipationMyAndToGrade(userId);
    }

    @GetMapping("/user/{uid}/gradeEvents/")
    public Iterable<Event> participationsToGrade(@PathVariable("uid") long userId){
        return eventService.getEventsToGrade(userId);
    }

}
