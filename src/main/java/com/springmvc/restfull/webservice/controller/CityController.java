package com.springmvc.restfull.webservice.controller;

import com.springmvc.restfull.webservice.model.City;
import com.springmvc.restfull.webservice.repositories.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class CityController {

    @Autowired
    CityRepository cityRepository;

    @PostMapping(path="/city")
    public @ResponseBody
    City addCity(@RequestBody City city){
        return cityRepository.save(city);
    }

    @GetMapping(path = "/cities")
    public @ResponseBody Iterable<City> getCities(){
        return cityRepository.findAll();
    }

}
