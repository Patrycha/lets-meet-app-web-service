package com.springmvc.restfull.webservice.controller;

import com.springmvc.restfull.webservice.model.Group;
import com.springmvc.restfull.webservice.model.Notification;
import com.springmvc.restfull.webservice.model.ParticipationStatus;
import com.springmvc.restfull.webservice.model.User;
import com.springmvc.restfull.webservice.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    IUserService userService;

    @GetMapping(path="/users/")
    public @ResponseBody Iterable<User> getAllUsers(){
        return userService.getAllUsersFromRepo();
    }

    @GetMapping(path="/user/{uid}/")
    public @ResponseBody User getUser(@PathVariable("uid") long userId){
        return userService.getUserById(userId);
    }

    @GetMapping(path="/user/{uid}/subscribing/{gid}")//ad :
    public @ResponseBody boolean isGroupMember(@PathVariable("uid") long uid, @PathVariable("gid") long gid){
        return userService.isSubscribing(uid, gid);
    }

    @GetMapping(path="/user/{uid}/organizer/{gid}")
    public @ResponseBody boolean isOrganizer(@PathVariable("uid") long uid, @PathVariable("gid") long gid){
        return userService.isOrganizer(uid, gid);
    }

    @PutMapping(path="/user/{userId}/subscribe/{gid}/")
    public @ResponseBody ResponseEntity<Void> setSubscribtion(@PathVariable long userId, @PathVariable("gid") long groupId){
        return userService.setSubscribtion(userId, groupId);
    }

    @DeleteMapping(path="/user/{uid}/unsubscribe/{gid}/")
    public @ResponseBody boolean unsetSubscribtion(@PathVariable("uid") long userId, @PathVariable("gid") long groupId){
        return userService.unsetSubscribtion(userId, groupId);
    }

    @GetMapping(path="/user/{uid}/groups/", params = {"category"})
    public @ResponseBody
    Iterable<Group> getSubscribingGroups(@RequestParam(value = "category") long categoryId, @PathVariable("uid") long userId){
        return userService.getSubscribingGroups(categoryId, userId);
    }

    @GetMapping(path = "/user/{uid}/participating/{eid}/")
    public ResponseEntity<ParticipationStatus> getParticipationStatus(@PathVariable("eid") long eventId, @PathVariable("uid") long userId){
        return userService.isParticipating(eventId, userId);
    }

    @GetMapping(path="/login/")
    public @ResponseBody
    ResponseEntity<User> login(@RequestHeader("Authorization") String header){
        User user = userService.login(header);
        if (user == null)
            return  new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    @PostMapping(path="/register/")
    public @ResponseBody
    ResponseEntity<User> register(@RequestBody User user){
        User result = userService.register(user);
        if (result == null)
            return  new ResponseEntity<User>(HttpStatus.CONFLICT);
        return new ResponseEntity<User>(result, HttpStatus.OK);
    }

    @GetMapping(path="/user/{uid}/notification/", produces="application/json")
    public @ResponseBody
    ResponseEntity<List<Notification>> getNotifications(@PathVariable("uid") long userId, @RequestParam("status") String statusName){
        return userService.getNotifications(userId, statusName);
    }

    @GetMapping(path = "/user/{uid}/group/")
    public @ResponseBody
    ResponseEntity<List<Group>> getGroups(@PathVariable("uid") long userId,
                                          @RequestParam(value = "isAdmin", defaultValue = "true") boolean isAdmin,
                                          @RequestParam(value = "isSubscriber", defaultValue = "true") boolean isSubscriber,
                                          @RequestParam(value = "categoryID", defaultValue = "-1") long categoryId){
        return userService.getGroups(userId, isAdmin, isSubscriber, categoryId);
    }

}
