package com.springmvc.restfull.webservice.controller;

import com.springmvc.restfull.webservice.model.Event;
import com.springmvc.restfull.webservice.model.Group;
import com.springmvc.restfull.webservice.model.User;
import com.springmvc.restfull.webservice.service.IGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
public class GroupController {

    @Autowired
    IGroupService groupService;

    @GetMapping(path="/group/")
    public Iterable<Group> getGroups(){
        return groupService.findAllGroups();
    }

    @GetMapping(path="/group/{id}/")
    public Group getGroupById(@PathVariable("id") long id){
        return groupService.findGroupById(id);
    }

    @GetMapping(path="/group/{gid}/user/")
    public List<User> getUsers(@PathVariable("gid") long groupId){
        return groupService.findUsers(groupId);
    }

    @PutMapping(path="/category/{cid}/group/")
    public @ResponseBody
    ResponseEntity<Void> addGroup(@PathVariable("cid") long categoryId, @RequestBody Group newGroup, @PathParam("userId") long userId){
       return groupService.addNewGroup(categoryId, newGroup, userId);
    }

    @GetMapping(path="/group/{gid}/event/")
    public Iterable<Event> getEvents(@PathVariable("gid") long groupId){
        return groupService.findEvents(groupId);
    }

    @GetMapping(path="/group/{gid}/subscriber/count/")
    public Integer getSubscribersCount(@PathVariable("gid") long groupId){
        return groupService.countSubscribers(groupId);
    }

    @GetMapping(path="/group/{gid}/event/count/")
    public Integer getEventsCount(@PathVariable("gid") long groupId){
        return groupService.countEvents(groupId);
    }
}
