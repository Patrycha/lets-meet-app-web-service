package com.springmvc.restfull.webservice.controller;

import com.springmvc.restfull.webservice.model.Notification;
import com.springmvc.restfull.webservice.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NotificationController {

    @Autowired
    NotificationService notificationService;

    @GetMapping(path="/notification/{nid}/cancel/", produces="application/json")
    public @ResponseBody
    ResponseEntity<Notification> cancelNotifications(@PathVariable("nid") long notificationId){
        return notificationService.cancelNotification(notificationId);
    }


}
