package com.springmvc.restfull.webservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.springmvc.restfull.webservice.validation.SexConstraint;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static com.springmvc.restfull.webservice.validation.Constants.*;

@Entity
@Table(name = "users")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ID")
    private long id;

    @Column(unique = true, length = USER_LOGIN_LENGTH_MAX, nullable = false)
    private String login;

    @Column(length = USER_PASSW_LENGTH_MAX, nullable = false)
    private String password;

    @Column(unique = true, length = USER_EMAIL_LENGTH_MAX, nullable = false)
    private String email;

    @Column(length = USER_NAME_LENGTH_MAX, nullable = false)
    private String name;

    @Column(length = USER_SURNAME_LENGTH_MAX, nullable = false)
    private String surname;

    @Column(name="birth_date", nullable = false)
    private Date birthDate;

    @Column(name="is_admin", nullable = false)
    private boolean isAdmin;

    @Column(length = USER_DESCRIPTION_LENGTH_MAX)
    private String description;

    @Column(nullable = false)
    @SexConstraint
    private char sex;

    @Column(name="joining_date", nullable = false)
    private Date joiningDate;

    @Column(name="image_path")
    private String imagePath;

    @Column(name="absences_count")
    private int absencesCount;

    @Column(name="cancellings_count")
    private int cancellingsCount; //after permissive time

    @Column(name="grades_count")
    private int gradesCount;

    private double average;

	@JsonIgnore
    @ManyToOne
    @JoinColumn(name="city_ID")
    private City city;

    @OneToMany(mappedBy = "administrator")
    private List<Group> administratesGroups;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "groups_users",joinColumns = {
            @JoinColumn(name = "user_ID", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "group_ID", nullable = false, updatable = false)
    })
    private List<Group> joinedGroups;

    @OneToMany(mappedBy = "organizer")
    private List<Event> organizedEvents;

    @OneToMany(mappedBy = "user")
    private List<Participation> participations;

    @JsonIgnore
    @OneToMany(mappedBy = "changeUser")
    private List<Participation> participationsChanged;

    @OneToMany(mappedBy = "user")
    private List<Notification> notifications;

    public List<Event> getOrganizedEvents() {
        return organizedEvents;
    }

    public List<Group> getJoinedGroups(){
        return joinedGroups == null ? new ArrayList<>() : joinedGroups;
    }

    public long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public City getCity() { return city; }

    public boolean isAdmin() {
        return isAdmin;
    }

    public String getDescription() {
        return description;
    }

    public char getSex() {
        return sex;
    }

    public Date getJoiningDate() {
        return joiningDate;
    }

    public String getImagePath() {
        return imagePath;
    }

    public int getAbsencesCount() {
        return absencesCount;
    }

    public int getCancellingsCount() {
        return cancellingsCount;
    }

    public int getGradesCount() {
        return gradesCount;
    }

    public double getAverage() {
        return average;
    }

    public List<Participation> getParticipations() {
        return participations;
    }

    public List<Participation> getParticipationsChanged() {
        return participationsChanged;
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public List<Group> getAdministratesGroups() {
        return administratesGroups;
    }

    public List<Group> getAdministeresGroups() {
        return administratesGroups == null ? new ArrayList<>() : administratesGroups;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setAdministeresGroups(List<Group> administeresGroups) {
        this.administratesGroups = administeresGroups;
    }

    public void setJoinedGroups(List<Group> joinedGroups) {
        this.joinedGroups = joinedGroups;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public void setJoiningDate(Date joiningDate) {
        this.joiningDate = joiningDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public void setAbsencesCount(int absencesCount) {
        this.absencesCount = absencesCount;
    }

    public void setCancellingsCount(int cancellingsCount) {
        this.cancellingsCount = cancellingsCount;
    }

    public void setGradesCount(int gradesCount) {
        this.gradesCount = gradesCount;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name){this.name = name;}

    public void setSurname(String surname){this.surname = surname;}

    public void setSex(char sex) {
        this.sex = sex;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User() {

    }
}

