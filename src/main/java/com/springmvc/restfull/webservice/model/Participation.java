package com.springmvc.restfull.webservice.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name="participations")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Participation implements Serializable{
    @Id
    @Column(name="ID")
    private long id;

    @Column(name="change_date")
    private Date date;

    @ManyToOne
    @JoinColumn(name="participation_status_ID")
    private ParticipationStatus status;

    @ManyToOne
    @JoinColumn(name="event_ID")
    private Event event;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="user_ID")
    private User user;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="change_user_ID")
    private User changeUser;

    @JsonProperty("thisUserMadeRecentChange")
    public boolean thisUserMadeRecentChange(){
        if(user != null && changeUser != null)
            return user.getId() == changeUser.getId();
        return false;
    }

    public long getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public ParticipationStatus getStatus() {
        return status;
    }

    public Event getEvent() {
        return event;
    }

    public User getUser() {
        return user;
    }

    public User getChangeUser() {
        return changeUser;
    }

    public void setStatus(ParticipationStatus status) {
        this.status = status;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setChangeUser(User changeUser) {
        this.changeUser = changeUser;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Participation(){

    }

    public Participation(long id, Date date, ParticipationStatus status, Event event, User user, User changeUser) {
        this.id = id;
        this.date = date;
        this.status = status;
        this.event = event;
        this.user = user;
        this.changeUser = changeUser;
    }
}
