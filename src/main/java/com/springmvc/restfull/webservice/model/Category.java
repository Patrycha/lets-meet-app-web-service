package com.springmvc.restfull.webservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.springmvc.restfull.webservice.validation.Constants.CATEGORY_DESCRIPTION_LENGTH_MAX;
import static com.springmvc.restfull.webservice.validation.Constants.CATEGORY_NAME_LENGTH_MAX;

@Entity
@Table(name = "categories")
public class Category implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column( name="ID")
    private long id;

    @Column(length = CATEGORY_NAME_LENGTH_MAX, nullable = false)
    private String name;

    @Column(length = CATEGORY_DESCRIPTION_LENGTH_MAX)
    private String description;

    @Column(name="creating_date", nullable = false)
    private Date creatingDate;

    @Column(name="image_path")
    private String imagePath;

    @JsonIgnore
    @OneToMany(mappedBy = "category")
    private List<Group> groups;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "city_ID")
    private City city;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatingDate() { return creatingDate;}

    public void setCreatingDate(Date creatingDate) { this.creatingDate = creatingDate;}

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public City getCity(){ return city;}

    public void setCity(City city){ this.city = city;}

    public Category(long id, String name, String description, String imagePath, City city) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.imagePath = imagePath;
        this.city = city;
        creatingDate = new Date(Calendar.getInstance().getTimeInMillis());
    }

    public Category(){
    }
}
