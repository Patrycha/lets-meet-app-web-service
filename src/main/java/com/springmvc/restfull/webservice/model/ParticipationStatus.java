package com.springmvc.restfull.webservice.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name="participation_status")
public class ParticipationStatus implements Serializable{
    @Id
    @Column(name="ID")
    private long id;

    private String name;

    @OneToMany(mappedBy = "status")
    private List<Participation> participations;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    ParticipationStatus(){}
}
