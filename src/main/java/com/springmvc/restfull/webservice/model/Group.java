package com.springmvc.restfull.webservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "groups")
public class Group implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ID")
    private long id;

    @Column(nullable = false, unique = true, length = 50)
    private String name;

    @Column(length = 255)
    private String description;

    @Column(name="creating_date", nullable = false)
    private Date creatingDate; //2016/11/16 12:08:43*/

    @Column(name="image_path", length = 200)
    private String imagePath;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "category_ID")
    private Category category;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "joinedGroups")
    private List<User> subscribers;

    public List<User> getSubscribers(){
        return subscribers;
    }

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="user_ID")
    private User administrator;

    public User getAdministrator() {
        return administrator;
    }

    public void setAdministrator(User administrator) {
        this.administrator = administrator;
    }

    @OneToMany(mappedBy = "group")
    private List<Event> events;

    public List<Event> getEvents() {
        return events;
    }

    public Group() {
        subscribers = new ArrayList<>();
    }

    public Group(long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
        //  creatingDate = new Date();
        imagePath = "";
    }
    public Group(long id, String name, String description, Date date) {
        this.id = id;
        this.name = name;
        this.description = description;
        //  creatingDate = date;
        imagePath = "";
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatingDate() {
        return creatingDate;
    }

    public void setCreatingDate(Date creatingDate) {
        this.creatingDate = creatingDate;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

   /* @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", imagePath='" + imagePath + '\'' +
                ", category=" + category +
                '}';
    }*/
   @JsonProperty("adminId")
   public long getAdminId(){return administrator == null? -1 : administrator.getId();}
}
