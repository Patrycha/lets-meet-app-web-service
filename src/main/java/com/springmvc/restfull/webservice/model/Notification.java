package com.springmvc.restfull.webservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "notifications")
public class Notification implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ID")
    private long id;

    @ManyToOne
    @JoinColumn(name="status_ID")
    private NotificationStatus status;

    @ManyToOne
    @JoinColumn(name="event_ID")
    private Event event;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="user_ID")
    private User user;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public NotificationStatus getStatus() {
        return status;
    }

    public void setStatus(NotificationStatus status) {
        this.status = status;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Notification(){
    }

    public Notification(NotificationStatus status, Event event, User user) {
        this.status = status;
        this.event = event;
        this.user = user;
    }
}
