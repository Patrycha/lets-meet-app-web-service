package com.springmvc.restfull.webservice.model;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.util.List;

@Entity
@Table(name="events")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Event implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ID")
    private long id;

    @Column(nullable = false)
    private String name;

    private String description;

    private String place;

    @Column(name="beginning_date")
    private Date beginningDate;

    @JsonFormat(pattern = "HH:mm:ss")
    @Column(name="beginning_time")
    private Time beginningTime;

    @Column(name="ending_date")
    private Date endingDate;

    @Column(name="ending_time")
    @JsonFormat(pattern = "HH:mm:ss")
    private Time endingTime;

    @Column(name="creating_date")
    private Date creatingDate;

    @Column(name="image_path")
    private String imagePath;

    @Column(name="participants_limit")
    private int participantsLimit;

    @ManyToOne
    @JoinColumn(name="visibility_ID")
    private VisibilityStatus visibility;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="group_ID")
    private Group group;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="organizer_ID")
    private User organizer;

    @OneToMany(mappedBy = "event")
    private List<Participation> participations;

    @JsonIgnore
    @OneToMany(mappedBy = "event")
    private List<Notification> notifications;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getPlace() {
        return place;
    }

    public Date getBeginningDate() {
        return beginningDate;
    }

    public Time getBeginningTime() {
        return beginningTime;
    }

    public Date getEndingDate() {
        return endingDate;
    }

    public Time getEndingTime() {
        return endingTime;
    }

    public Date getCreatingDate() {
        return creatingDate;
    }

    public String getImagePath() {
        return imagePath;
    }

    public int getParticipantsLimit() {
        return participantsLimit;
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    @JsonProperty("organizerId")
    public long getOrganizerId(){return organizer == null ? -1 : organizer.getId();}

    @JsonProperty("organizerNameSurname")
    public String getOrganizerDetails(){
        return organizer != null ? organizer.getName() + " " + organizer.getSurname() : "";}

    public VisibilityStatus getVisibility() {
        return visibility;
    }

    public Group getGroup() {
        return group;
    }

    public User getOrganizer() {
        return organizer;
    }

    public List<Participation> getParticipations() {
        return participations;
    }

    @JsonProperty("participationCounter")
    public int getParticipationsCount(){
        int result = 0;
        if(participations != null){
            for(Participation p : participations){
                if (p.getStatus().getName().equals("accepted"))
                    result++;
            }
        }
        return result;
    }

    @JsonProperty("groupName")
    public String getGroupName(){
        return group == null ? "": group.getName();
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public void setBeginningDate(Date beginningDate) {
        this.beginningDate = beginningDate;
    }

    public void setBeginningTime(Time beginningTime) {
        this.beginningTime = beginningTime;
    }

    public void setEndingDate(Date endingDate) {
        this.endingDate = endingDate;
    }

    public void setEndingTime(Time endingTime) {
        this.endingTime = endingTime;
    }

    public void setCreatingDate(Date creatingDate) {
        this.creatingDate = creatingDate;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void setParticipantsLimit(int participantsLimit) {
        this.participantsLimit = participantsLimit;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public void setOrganizer(User organizer) {
        this.organizer = organizer;
    }

    public void setVisibility(VisibilityStatus visibility) {
        this.visibility = visibility;
    }

    public Event(){}
}
