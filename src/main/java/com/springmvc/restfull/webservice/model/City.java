package com.springmvc.restfull.webservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "cities")
public class City implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ID")
    private long id;

    @Column(nullable = false)
    private String country;

    @Column(name="city_name", nullable = false)
    private String cityName;

    @JsonIgnore
    @OneToMany(mappedBy = "city")
    private List<User> users;

    @OneToMany(mappedBy = "city")
    private List<Category> categories;


    public List<User> getUsers() {
        return users;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public long getId() {
        return id;
    }

    public String getCountry() {
        return country;
    }

    public String getCityName() {
        return cityName;
    }

    /* setters methods */

    public void setId(long id) {
        this.id = id;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    /* constructors */

    public City() {
    }

    public City(long id, String country, String cityName, List<User> users, List<Category> categories) {
        this.id = id;
        this.country = country;
        this.cityName = cityName;
        this.users = users;
        this.categories = categories;
    }

    public City(String country, String cityName) {
        this.country = country;
        this.cityName = cityName;
    }

}
