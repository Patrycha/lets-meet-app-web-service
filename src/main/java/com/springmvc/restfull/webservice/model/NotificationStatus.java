package com.springmvc.restfull.webservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "notification_status")
public class NotificationStatus implements Serializable{
    @Id
    @Column(name="ID")
    private long id;

    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "status")
    private List<Notification> notifications;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public NotificationStatus(){}

    public NotificationStatus(String name){ this.name = name; }

}
