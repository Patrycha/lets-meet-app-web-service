package com.springmvc.restfull.webservice.repositories;

import com.springmvc.restfull.webservice.model.Participation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParticipationRepository extends CrudRepository<Participation, Long> {
}
