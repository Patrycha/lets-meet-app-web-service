package com.springmvc.restfull.webservice.repositories;

import com.springmvc.restfull.webservice.model.VisibilityStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VisibilityStatusRepository extends CrudRepository<VisibilityStatus, Long>{
    VisibilityStatus findByName(String name);
}
