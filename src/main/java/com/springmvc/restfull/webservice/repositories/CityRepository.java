package com.springmvc.restfull.webservice.repositories;

import com.springmvc.restfull.webservice.model.City;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends CrudRepository<City, Long>{
}
