package com.springmvc.restfull.webservice.repositories;

import com.springmvc.restfull.webservice.model.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {
}
