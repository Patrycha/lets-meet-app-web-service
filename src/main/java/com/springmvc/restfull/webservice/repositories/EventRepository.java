package com.springmvc.restfull.webservice.repositories;

import com.springmvc.restfull.webservice.model.Event;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventRepository extends CrudRepository<Event, Long>{
    Event findByName(String name);
}
