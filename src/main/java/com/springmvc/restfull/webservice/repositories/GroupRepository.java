package com.springmvc.restfull.webservice.repositories;

import com.springmvc.restfull.webservice.model.Group;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends CrudRepository<Group, Long>{
}
