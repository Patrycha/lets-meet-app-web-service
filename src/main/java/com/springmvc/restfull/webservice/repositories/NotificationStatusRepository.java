package com.springmvc.restfull.webservice.repositories;

import com.springmvc.restfull.webservice.model.NotificationStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationStatusRepository extends CrudRepository<NotificationStatus, Long>{
    NotificationStatus findByName(String name);
}
