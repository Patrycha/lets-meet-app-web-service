package com.springmvc.restfull.webservice.repositories;

import com.springmvc.restfull.webservice.model.Notification;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationRepository extends CrudRepository<Notification, Long> {
}
