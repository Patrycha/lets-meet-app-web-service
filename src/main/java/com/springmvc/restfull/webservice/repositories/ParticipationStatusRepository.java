package com.springmvc.restfull.webservice.repositories;

import com.springmvc.restfull.webservice.model.ParticipationStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParticipationStatusRepository extends CrudRepository<ParticipationStatus, Long>{
    ParticipationStatus findByName(String name);
}
