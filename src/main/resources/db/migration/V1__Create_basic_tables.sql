CREATE TABLE participation_status (ID int AUTO_INCREMENT NOT NULL, name varchar(20) NOT NULL UNIQUE, PRIMARY KEY (ID));
CREATE TABLE friendship_status (ID int AUTO_INCREMENT NOT NULL, name varchar(20) NOT NULL UNIQUE, PRIMARY KEY (ID));
CREATE TABLE visibility_status (ID int AUTO_INCREMENT NOT NULL, name varchar(20) NOT NULL UNIQUE, PRIMARY KEY (ID));
CREATE TABLE cities (ID int AUTO_INCREMENT NOT NULL, country varchar(30) NOT NULL, city_name varchar(30) NOT NULL, PRIMARY KEY (ID));

CREATE TABLE users (ID int AUTO_INCREMENT NOT NULL, city_ID int NOT NULL, login varchar(30) NOT NULL UNIQUE, password varchar(30) NOT NULL, email varchar(255) NOT NULL UNIQUE, name varchar(30) NOT NULL, surname varchar(30) NOT NULL, birth_date date NOT NULL, is_admin bit NOT NULL, description varchar(255) NULL, sex char(1) NOT NULL, joining_date date NOT NULL, image_path varchar(200) NULL, absences_count int NULL, cancellings_count int NULL, grades_count int NULL, average real NULL, PRIMARY KEY (ID));
CREATE TABLE categories (ID int AUTO_INCREMENT NOT NULL, city_ID int NOT NULL, name varchar(50) NOT NULL, description varchar(255) NULL, creating_date date NOT NULL, image_path varchar(200) NULL, PRIMARY KEY (ID));
ALTER TABLE users ADD CONSTRAINT lives FOREIGN KEY (city_ID) REFERENCES cities (ID);
ALTER TABLE categories ADD CONSTRAINT concern FOREIGN KEY (city_ID) REFERENCES cities (ID);

CREATE TABLE groups (ID int AUTO_INCREMENT NOT NULL, category_ID int NOT NULL, user_ID int NOT NULL, name varchar(50) NOT NULL UNIQUE, description varchar(255) NULL, creating_date date NOT NULL, image_path varchar(200) NULL, PRIMARY KEY (ID));
ALTER TABLE groups ADD CONSTRAINT administers FOREIGN KEY (user_ID) REFERENCES users (ID);
ALTER TABLE groups ADD CONSTRAINT belongs_in FOREIGN KEY (category_ID) REFERENCES categories (ID);

CREATE TABLE groups_users (user_ID int NOT NULL, group_ID int NOT NULL, PRIMARY KEY (user_ID, group_ID));
ALTER TABLE groups_users ADD CONSTRAINT subscribes1 FOREIGN KEY (user_ID) REFERENCES users (ID);
ALTER TABLE groups_users ADD CONSTRAINT subscribes2 FOREIGN KEY (group_ID) REFERENCES groups (ID);

CREATE TABLE events (ID int AUTO_INCREMENT NOT NULL, visibility_ID int NOT NULL, group_ID int NOT NULL, organizer_ID int NOT NULL, name varchar(50) NOT NULL, description varchar(255) NULL, place varchar(100) NULL, beginning_date date NULL, ending_date date NULL, beginning_time time NULL, ending_time time NULL, creating_date date NOT NULL, image_path varchar(200) NULL, participants_limit mediumint NOT NULL, PRIMARY KEY (ID));
ALTER TABLE events ADD CONSTRAINT organizes FOREIGN KEY (organizer_ID) REFERENCES users (ID);
ALTER TABLE events ADD CONSTRAINT belongs_in2 FOREIGN KEY (group_ID) REFERENCES groups (ID);
ALTER TABLE events ADD CONSTRAINT have FOREIGN KEY (visibility_ID) REFERENCES visibility_status (ID);


CREATE TABLE participations (ID int AUTO_INCREMENT NOT NULL, participation_status_ID int NOT NULL, change_date date NOT NULL, event_ID int NOT NULL, user_ID int NOT NULL, change_user_ID int NOT NULL, PRIMARY KEY (ID));
ALTER TABLE participations ADD CONSTRAINT participation_events FOREIGN KEY (event_ID) REFERENCES events (ID);
ALTER TABLE participations ADD CONSTRAINT participation_users FOREIGN KEY (user_ID) REFERENCES users (ID);
ALTER TABLE participations ADD CONSTRAINT participation_change_user FOREIGN KEY (change_user_ID) REFERENCES users (ID);
ALTER TABLE participations ADD CONSTRAINT participation_status FOREIGN KEY (participation_status_ID) REFERENCES participation_status (ID);

INSERT INTO visibility_status (name) VALUES ('public'), ('private');
INSERT INTO friendship_status (name) VALUES ('pending'), ('accepted'), ('declined'), ('blocked');
INSERT INTO participation_status (name) VALUES ('pending'), ('accepted'), ('declined'), ('cancelled'),('present-affirmed'), ('present-not affirmed'), ('absent-affirmed'), ('absent-not affirmed') ;