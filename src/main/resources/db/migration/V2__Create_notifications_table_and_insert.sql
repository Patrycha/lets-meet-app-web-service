CREATE TABLE notification_status (ID int AUTO_INCREMENT NOT NULL, name varchar(20) NOT NULL UNIQUE, PRIMARY KEY (ID));
CREATE TABLE notifications ( ID int AUTO_INCREMENT NOT NULL, event_ID int NOT NULL, user_ID int NOT NULL, status_ID int NOT NULL, PRIMARY KEY (ID));

ALTER TABLE notifications ADD CONSTRAINT notif_status_fk FOREIGN KEY (status_ID) REFERENCES users (ID);
ALTER TABLE notifications ADD CONSTRAINT notif_event_fk FOREIGN KEY (event_ID) REFERENCES events (ID);
ALTER TABLE notifications ADD CONSTRAINT notif_user_fk FOREIGN KEY (user_ID) REFERENCES users (ID);

INSERT INTO notification_status (name) VALUES ('created'), ('cancelled');