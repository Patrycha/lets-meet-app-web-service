package com.springmvc.restfull.webservice;

import com.springmvc.restfull.webservice.model.Category;
import com.springmvc.restfull.webservice.model.City;
import com.springmvc.restfull.webservice.model.Group;
import com.springmvc.restfull.webservice.model.User;
import com.springmvc.restfull.webservice.repositories.GroupRepository;
import com.springmvc.restfull.webservice.repositories.UserRepository;
import com.springmvc.restfull.webservice.service.UserService;
import com.springmvc.restfull.webservice.util.TestDataGenerator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

import static org.mockito.Matchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private GroupRepository groupRepository;

    @InjectMocks
    private UserService userService;

    private static List<User> users;

    private static TestDataGenerator testDataGenerator;

    @BeforeClass
    public static void setupAll() throws Exception {
        testDataGenerator = new TestDataGenerator();

        City city = testDataGenerator.getRandomCity();
        users = testDataGenerator.getRandomUserList(city, 3);
        Category category = testDataGenerator.getRandomCategory(city);

        List<Group> groups = testDataGenerator.getRandomGroupList(category, 2);
        for ( int i = 0; i < groups.size(); i++){
            groups.get(i).setId(i + 1);
        }
        users.get(0).setAdministeresGroups(groups);
        groups = testDataGenerator.getRandomGroupList(category, 4);
        for ( int i = 0; i < groups.size(); i++){
            groups.get(i).setId(i + 1 + users.get(0).getAdministeresGroups().size());
        }
        users.get(1).setAdministeresGroups(groups);

        users.get(0).setJoinedGroups(users.get(1).getAdministeresGroups());
        users.get(1).setJoinedGroups(users.get(0).getAdministeresGroups());
    }

    @Before
    public void setupEach(){
        Mockito.when(userRepository.findOne(1l)).thenAnswer(
                new Answer() {
                    public User answer(InvocationOnMock invocation) {
                        return users.get(0);
                    }
                });

        Mockito.when(userRepository.findOne(2l)).thenAnswer(
                new Answer() {
                    public User answer(InvocationOnMock invocation) {
                        return users.get(1);
                    }
                });
        Mockito.when(userRepository.findOne(3l)).thenAnswer(
                new Answer() {
                    public User answer(InvocationOnMock invocation) {
                        return users.get(2);
                    }
                });
        Mockito.when(userRepository.findOne(100l)).thenAnswer(
                new Answer() {
                    public User answer(InvocationOnMock invocation) {
                        return null;
                    }
                });
        Mockito.when(groupRepository.findOne(100l)).thenAnswer(
                new Answer() {
                    public Group answer(InvocationOnMock invocation) {
                        return null;
                    }
                });
        Mockito.when(groupRepository.findOne(1l)).thenAnswer(
                new Answer() {
                    public Group answer(InvocationOnMock invocation) {
                        return users.get(0).getAdministeresGroups().get(0);
                    }
                });
        Mockito.when(userRepository.save((User) any())).thenAnswer(
                new Answer() {
                    public User answer(InvocationOnMock invocation) {
                        return null;
                    }
                });
    }

    @Test
    public void should_findGroups_when_userExists(){
        ResponseEntity<List<Group>> responseEntity = userService.getGroups(1l, true, true, -1);
        assert (responseEntity.getStatusCode().equals(HttpStatus.OK));
    }

    @Test
    public void should_failToFindGroups_when_userDoesNotExist(){
        ResponseEntity<List<Group>> responseEntity = userService.getGroups(100l, true, true, -1);
        assert (responseEntity.getStatusCode().equals(HttpStatus.NOT_FOUND));
    }

    @Test
    public void should_failToJoin_when_userHasAlreadyJoinedGroup(){
        ResponseEntity<Void> responseEntity = userService.setSubscribtion(1l, 3l);
        assert (responseEntity.getStatusCode().equals(HttpStatus.CONFLICT));
    }

    @Test
    public void should_failToJoin_when_userAdministersGroup(){
        ResponseEntity<Void> responseEntity = userService.setSubscribtion(1l, 1l);
        assert (responseEntity.getStatusCode().equals(HttpStatus.CONFLICT));
    }

    @Test
    public void should_failToJoin_when_userDoesNotExist(){
        ResponseEntity<Void> responseEntity = userService.setSubscribtion(100l, 1l);
        assert (responseEntity.getStatusCode().equals(HttpStatus.NOT_FOUND));
    }

    @Test
    public void should_failToJoin_when_groupDoesNotExist(){
        ResponseEntity<Void> responseEntity = userService.setSubscribtion(1l, 100l);
        assert (responseEntity.getStatusCode().equals(HttpStatus.NOT_FOUND));
    }

    @Test
    public void should_join_when_allConditionsMet(){
        ResponseEntity<Void> responseEntity = userService.setSubscribtion(3l, 1l);
        assert (responseEntity.getStatusCode().equals(HttpStatus.CREATED));
    }
}
