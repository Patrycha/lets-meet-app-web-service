package com.springmvc.restfull.webservice;

import com.springmvc.restfull.webservice.model.Category;
import com.springmvc.restfull.webservice.model.City;
import com.springmvc.restfull.webservice.model.Group;
import com.springmvc.restfull.webservice.model.User;
import com.springmvc.restfull.webservice.repositories.CategoryRepository;
import com.springmvc.restfull.webservice.repositories.GroupRepository;
import com.springmvc.restfull.webservice.repositories.UserRepository;
import com.springmvc.restfull.webservice.service.GroupService;
import com.springmvc.restfull.webservice.util.TestDataGenerator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class GroupServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private GroupRepository groupRepository;

    @Mock
    private CategoryRepository categoryRepository;

    @InjectMocks
    private GroupService groupService;

    private static Category category;

    private static User user;

    private static TestDataGenerator testDataGenerator;

    @BeforeClass
    public static void setupAll() throws Exception {
        testDataGenerator = new TestDataGenerator();

        City city = testDataGenerator.getRandomCity();
        category = testDataGenerator.getRandomCategory(city);
        user = testDataGenerator.getRandomUser(city);
    }

    @Before
    public void setupEach(){
        Mockito.when(userRepository.findOne(1l)).thenAnswer(
                new Answer() {
                    public User answer(InvocationOnMock invocation) {
                        return user;
                    }
                });
        Mockito.when(categoryRepository.findOne(1l)).thenAnswer(
                new Answer() {
                    public Category answer(InvocationOnMock invocation) {
                        return category;
                    }
                });
    }

    @Test
    public void should_failToCreateGroups_when_categoryDoesNotExist(){
        Group newGroup = testDataGenerator.getRandomGroup(category);
        ResponseEntity<Void> responseEntity = groupService.addNewGroup(2l, newGroup, 1l);
        assert (responseEntity.getStatusCode().equals(HttpStatus.NOT_FOUND));
    }

    @Test
    public void should_failToCreateGroup_when_userDoesNotExist(){
        Group newGroup = testDataGenerator.getRandomGroup(category);
        ResponseEntity<Void> responseEntity = groupService.addNewGroup(1l, newGroup, 2l);
        assert (responseEntity.getStatusCode().equals(HttpStatus.NOT_FOUND));
    }

    @Test
    public void should_createGroup_when_allConditionsMet(){
        Group newGroup = testDataGenerator.getRandomGroup(category);
        ResponseEntity<Void> responseEntity = groupService.addNewGroup(1l, newGroup, 1l);
        assert (responseEntity.getStatusCode().equals(HttpStatus.CREATED));
    }
}
