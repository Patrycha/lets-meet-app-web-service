package com.springmvc.restfull.webservice;

import com.springmvc.restfull.webservice.model.*;
import com.springmvc.restfull.webservice.repositories.NotificationRepository;
import com.springmvc.restfull.webservice.repositories.NotificationStatusRepository;
import com.springmvc.restfull.webservice.service.NotificationService;
import com.springmvc.restfull.webservice.util.TestDataGenerator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class NotificationServiceTest {

    @Mock
    private NotificationStatusRepository notificationStatusRepository;

    @Mock
    private NotificationRepository notificationRepository;

    @InjectMocks
    private NotificationService notificationService;

    private static TestDataGenerator testDataGenerator;

    private static List<NotificationStatus> statuses;

    private static Notification notification;

    @BeforeClass
    public static void setupAll() throws Exception {
        testDataGenerator = new TestDataGenerator();

        City city = testDataGenerator.getRandomCity();
        Category category = testDataGenerator.getRandomCategory(city);
        User user = testDataGenerator.getRandomUser(city);
        statuses = new ArrayList<>();
        statuses.add(new NotificationStatus("created"));
        statuses.add(new NotificationStatus("cancelled"));

        Event event = testDataGenerator.getRandomEvent(testDataGenerator.getRandomGroup(category), user, new VisibilityStatus("public"));
        notification = testDataGenerator.getRandomNotification(event, statuses.get(0));
    }

    @Before
    public void setupEach(){
        Mockito.when(notificationRepository.findOne(1l)).thenAnswer(
                new Answer() {
                    public Notification answer(InvocationOnMock invocation) {
                        return notification;
                    }
                });
        Mockito.when(notificationStatusRepository.findByName("cancelled")).thenAnswer(
                new Answer() {
                    public NotificationStatus answer(InvocationOnMock invocation) {
                        return statuses.get(1);
                    }
                });
    }

    @Test
    public void should_failToCancel_when_NotificationDoesNotExist(){
        ResponseEntity<Notification> responseEntity = notificationService.cancelNotification(2l);
        assert (responseEntity.getStatusCode().equals(HttpStatus.NOT_FOUND));
    }

    @Test
    public void should_cancelNotification_when_notificationExists(){
        ResponseEntity<Notification> responseEntity = notificationService.cancelNotification(1l);
        assert (responseEntity.getStatusCode().equals(HttpStatus.OK));
    }
}
