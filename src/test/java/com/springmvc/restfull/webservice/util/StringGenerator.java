package com.springmvc.restfull.webservice.util;

import java.security.SecureRandom;
import java.util.Locale;
import java.util.Random;
import java.util.Objects;

public class StringGenerator {
    /**
     * Generate a random string.
     */
    public String nextString() {
        for (int i = 0; i < buffer.length; ++i)
            buffer[i] = symbols[random.nextInt(symbols.length)];
        return new String(buffer);
    }

    public static final String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static final String lower = upper.toLowerCase(Locale.ROOT);

    public static final String digits = "0123456789";

    public static final String alphanum = upper + lower + digits;

    private final Random random;

    private final char[] symbols;

    private final char[] buffer;

    public StringGenerator(int length, Random random, String symbols) {
        if (length < 1) throw new IllegalArgumentException();
        if (symbols.length() < 2) throw new IllegalArgumentException();
        this.random = Objects.requireNonNull(random);
        this.symbols = symbols.toCharArray();
        this.buffer = new char[length];
    }

    /**
     * Create an alphanumeric string generator.
     */
    public StringGenerator(int length, Random random) {
        this(length, random, alphanum);
    }

    /**
     * Create an alphanumeric strings from a secure generator.
     */
    public StringGenerator(int length) {
        this(length, new SecureRandom());
    }

    /**
     * Create session identifiers.
     */
    public StringGenerator() {
        this(21);
    }

}
