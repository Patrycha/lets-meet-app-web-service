package com.springmvc.restfull.webservice.util;

import com.springmvc.restfull.webservice.model.*;

import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

public class TestDataGenerator {
    private static final int STRING_LENGTH = 10;

    private StringGenerator randomString = new StringGenerator(STRING_LENGTH, new Random(), StringGenerator.upper + StringGenerator.lower);

    public City getRandomCity() {
        City city = new City();
        city.setCityName(randomString.nextString());
        city.setCountry(randomString.nextString());
        return city;
    }

    public User getRandomUser(City city){
        User user = new User();
        user.setLogin(randomString.nextString());
        user.setPassword(randomString.nextString());
        user.setEmail(randomString.nextString() + "@gmail.com");
        user.setName(randomString.nextString());
        user.setSurname(randomString.nextString());
        user.setAdmin(false);
        user.setJoiningDate( new Date(Calendar.getInstance().getTimeInMillis()));
        user.setSex('M');
        user.setBirthDate(Date.valueOf(LocalDate.now().minusYears(20)));
        user.setCity(city);
        return user;
    }

    public List<User> getRandomUserList(City city, int listSize) {
        List<User> users = new ArrayList<>(listSize);
        for ( int i = 0; i < listSize ; i++){
            users.add(getRandomUser(city));
        }
        return users;
    }

    public Category getRandomCategory(City city) {
        Category category = new Category();
        category.setName(randomString.nextString());
        category.setDescription(randomString.nextString());
        category.setCreatingDate(new Date(Calendar.getInstance().getTimeInMillis()));
        category.setCity(city);
        return category;
    }

    public List<Category> getRandomCategoryList(City city, int listSize) {
        List<Category> categories = new ArrayList<>(listSize);
        for ( int i = 0; i < listSize ; i++){
            categories.add(getRandomCategory(city));
        }
        return categories;
    }

    public Group getRandomGroup(Category category) {
        Group group = new Group();
        group.setCategory(category);
        group.setName(randomString.nextString());
        group.setCreatingDate(new Date(Calendar.getInstance().getTimeInMillis()));
        return group;
    }

    public List<Group> getRandomGroupList(Category category, int listSize) {
        List<Group> groups = new ArrayList<>(listSize);
        for ( int i = 0; i < listSize ; i++){
            groups.add(getRandomGroup(category));
        }
        return groups;
    }

    public Event getRandomEvent(Group group, User user, VisibilityStatus visibility) {
        Event event = new Event();
        event.setGroup(group);
        event.setName(randomString.nextString());
        event.setCreatingDate(new Date(Calendar.getInstance().getTimeInMillis()));
        event.setBeginningDate(new Date(Calendar.getInstance().getTimeInMillis()));
        event.setEndingDate(new Date(Calendar.getInstance().getTimeInMillis()));
        event.setBeginningTime(new Time(Calendar.getInstance().getTimeInMillis()));
        event.setEndingTime(new Time(Calendar.getInstance().getTimeInMillis()));
        event.setParticipantsLimit(10);
        event.setPlace(randomString.nextString());
        event.setDescription(randomString.nextString());
        event.setVisibility(visibility);
        event.setOrganizer(user);

        return event;
    }

    public Notification getRandomNotification(Event event, NotificationStatus status) {
        Notification notification = new Notification();
        notification.setEvent(event);
        notification.setStatus(status);
        return notification;
    }
}
