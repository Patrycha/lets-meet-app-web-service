package com.springmvc.restfull.webservice;

import com.springmvc.restfull.webservice.model.City;
import com.springmvc.restfull.webservice.model.User;
import com.springmvc.restfull.webservice.repositories.CityRepository;
import com.springmvc.restfull.webservice.repositories.UserRepository;
import com.springmvc.restfull.webservice.util.StringGenerator;
import com.springmvc.restfull.webservice.util.TestDataGenerator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.validation.ConstraintViolationException;
import java.security.SecureRandom;
import java.util.Random;

import static com.springmvc.restfull.webservice.validation.Constants.USER_LOGIN_LENGTH_MAX;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UserDBTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CityRepository cityRepository;

    private StringGenerator stringGenerator;

    private TestDataGenerator testDataGenerator = new TestDataGenerator();

    private City city;

    @Before
    public void setup() throws Exception {
        cityRepository.save(new City("country","city"));
        city = cityRepository.findOne(1l);
    }

    @Test
    public void should_saveUser_when_uniqueData(){

        User userToSave = testDataGenerator.getRandomUser(city);

        userRepository.save(userToSave);
        assert(userRepository.findByEmail(userToSave.getEmail()) != null); //email has to be unique
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void should_throwException_when_noUniqueLogin(){
       should_saveUser_when_uniqueData();

       User user = testDataGenerator.getRandomUser(city);
       user.setLogin(userRepository.findOne(1l).getLogin());

       userRepository.save(user);
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void should_throwException_when_noUniqueEmail(){
        should_saveUser_when_uniqueData();

        User user = testDataGenerator.getRandomUser(city);
        user.setEmail(userRepository.findOne(1l).getEmail());

        userRepository.save(user);
    }

    @Test
    public void should_saveUser_when_loginLengthEqualMax(){//todo
        stringGenerator = new StringGenerator(USER_LOGIN_LENGTH_MAX, new Random(), StringGenerator.upper);

        User user = testDataGenerator.getRandomUser(city);
        user.setLogin(stringGenerator.nextString());

        boolean thrown = false;
        try {
            userRepository.save(user);
        }catch(DataIntegrityViolationException e){
            thrown = true;
        }

        Assert.assertFalse(thrown);
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void should_throwException_when_loginLengthAboveMax(){
        stringGenerator = new StringGenerator(USER_LOGIN_LENGTH_MAX + 1, new SecureRandom(), StringGenerator.upper);

        User user = testDataGenerator.getRandomUser(city);
        user.setLogin(stringGenerator.nextString());

        userRepository.save(user);
    }

    @Test(expected = ConstraintViolationException.class)
    public void should_throwException_when_wrongSex(){

        User user = testDataGenerator.getRandomUser(city);
        user.setSex('X');

        userRepository.save(user);
    }

}
